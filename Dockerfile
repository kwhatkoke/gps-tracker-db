# Adjust PostgreSQL configuration so that remote connections to the
# database are possible.
FROM postgres
RUN echo "host all  all    0.0.0.0/0  md5" >> /var/lib/postgresql/data/pg_hba.conf

# And add ``listen_addresses`` to ``/var/lib/postgresql/data/postgresql.conf``
RUN echo "listen_addresses='*'" >> /var/lib/postgresql/data/postgresql.conf
RUN echo "max_connections = 300" >> /var/lib/postgresql/data/postgresql.conf
RUN echo "shared_buffers = 128MB" >> /var/lib/postgresql/data/postgresql.conf
RUN echo "effective_cache_size = 384MB" >> /var/lib/postgresql/data/postgresql.conf
RUN echo "maintenance_work_mem = 32MB" >> /var/lib/postgresql/data/postgresql.conf
RUN echo "checkpoint_completion_target = 0.9" >> /var/lib/postgresql/data/postgresql.conf
RUN echo "default_statistics_target = 100" >> /var/lib/postgresql/data/postgresql.conf
RUN echo "random_page_cost = 1.1" >> /var/lib/postgresql/data/postgresql.conf
RUN echo "effective_io_concurrency = 200" >> /var/lib/postgresql/data/postgresql.conf
RUN echo "work_mem = 655kB" >> /var/lib/postgresql/data/postgresql.conf
RUN echo "min_wal_size = 1GB" >> /var/lib/postgresql/data/postgresql.conf
RUN echo "max_wal_size = 4GB" >> /var/lib/postgresql/data/postgresql.conf
